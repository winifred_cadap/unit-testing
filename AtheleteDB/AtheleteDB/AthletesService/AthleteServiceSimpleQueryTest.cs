﻿using System;
using CAMS.DD.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CAMS.DD.Core.Services.Interfaces;
using System.Collections.Generic;
using CAMS.DD.Core.ViewModels.Athletes;
using System.Linq;

using Moq;

namespace CAMS.DD.Tests.AthletesService
{
    [TestClass]
    public class AthleteServiceSimpleQueryTest : AthleteServiceBase
    {
        [TestInitialize]
        public void TestInitialize()
        {
            InitTestData();
        }

        [TestMethod]
        [TestCategory("GetAllAthletes()")]
        public void AthleteService_GetallAthletes_If_It_Returns_Records()
        {
            var athletes = athleteService.GetAllAthletes();
            Assert.IsTrue(athletes.Athletes.Count > 0);
        }

        #region SearchAllAthletes(string q, int? athleteId, int retrieveSize = 10)

        [TestMethod]
        [TestCategory("SearchAllAthletes")]
        public void AthleteService_SearchAllAthletes_ById_Found_ShouldReturnSingleRecord()
        {
            IEnumerable<AthleteList> athletes = athleteService.SearchAllAthletes("", AthletId);
            Assert.IsTrue(athletes.Count() == 1);
        }

        [TestMethod]
        [TestCategory("SearchAllAthletes")]
        public void AthleteService_SearchAllAthletes_ById_NotFound_ShouldReturnZeroRecord()
        {
            IEnumerable<AthleteList> athletes = athleteService.SearchAllAthletes("", -1);
            Assert.IsTrue(athletes.Count() == 0);
        }

        [TestMethod]
        [TestCategory("SearchAllAthletes")]
        public void AthleteService_SearchAllAthletes_By_Name_Found_ShouldReturnRecords()
        {
            IEnumerable<AthleteList> athletes = athleteService.SearchAllAthletes(AthleteName, null);
            Assert.IsTrue(athletes.Count() > 1);
        }

        [TestMethod]
        [TestCategory("SearchAllAthletes")]
        public void AthleteService_SearchAllAthletes_ByName_NotFound_ShouldReturnZero()
        {
            IEnumerable<AthleteList> athletes = athleteService.SearchAllAthletes("jjd@##@#!0093ZZZ", null);
            Assert.IsTrue(athletes.Count() == 0);
        }

        [TestMethod]
        [TestCategory("SearchAllAthletes")]
        public void AthleteService_SearchAllAthletes_GetRecords_By10_ShouldReturn10Records()
        {
            IEnumerable<AthleteList> athletes = athleteService.SearchAllAthletes("", null, 10);
            Assert.IsTrue(athletes.Count() == 10);
        }

        #endregion

        #region GetAthlete(int id, int? userId = null, bool includeWatched = false)

        [TestMethod]
        [TestCategory("GetAthlete")]
        public void AthleteService_GetAthlete_Found_ShouldReturnModel()
        {
            var athlete = athleteServiceMock.GetAthlete(AthletId, null);
            System.Diagnostics.Debug.WriteLine(string.Format("Athele Name : {0}", athlete.Name));
            Assert.IsNotNull(athlete != null);
        }

        [TestMethod]
        [TestCategory("GetAthlete")]
        [ExpectedException(typeof(Exception))]
        public void AthleteService_GetAthlete_NotFound_ShouldThrowException()
        {
            try
            {
                var athlete = athleteService.GetAthlete(-1, null);

            }
            catch (Exception ex)
            {
                Assert.AreEqual("Athlete not found", ex.Message);
                throw;
            }
        }

        #endregion

        #region GetAthleteByUserId(int userId, bool includeWatched = false)

        [TestMethod]
        [TestCategory("GetAthleteByUserId")]
        public void AthleteService_GetAthleteByUserId_Found_ShouldReturnRecordInModel()
        {
            var athlete = athleteService.GetAthleteByUserId(UserId, false);
            Assert.IsNotNull(athlete != null);
        }

        [TestMethod]
        [TestCategory("GetAthleteByUserId")]
        public void AthleteService_GetAthleteByUserId_NotFound_ShouldReturnNull()
        {
            var athlete = athleteService.GetAthleteByUserId(-1, false);
            Assert.IsNull(athlete);
        }

        [TestMethod]
        [TestCategory("GetAthleteByUserId")]
        public void AthleteService_GetAthleteByUserId_Found_Watch_ShouldReturnRecord()
        {
            var athlete = athleteService.GetAthleteByUserId(UserId, true);
            
            Assert.IsNotNull(athlete);
        }

        #endregion

    }
       
}
