﻿using CAMS.DD.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CAMS.DD.Core.Services.Interfaces;
using CAMS.DD.Core.ViewModels.Athletes;
using System.Linq;
using PagedList;
using Moq;

namespace CAMS.DD.Tests.AthletesService
{
    [TestClass]
    public class AthleteServiceComplexQueryTest : AthleteServiceBase
    {
        [TestInitialize]
        public void TestInitialize()
        {
            InitTestData();
        }

        [TestMethod]
        [TestCategory("GetAllAthletes")]
        public void AthleteService_GetAllAthletes_Default_ShouldReturn_Records()
        {
            IPagedList<AthleteModel> athletes = athleteServiceMock.GetAllAthletes("", null, null, true, "",
                1, null, "name asc", 0, UserId, true, false, 1, 20);
            System.Diagnostics.Debug.WriteLine(string.Format("# of Records : {0}", athletes.Count));
            Assert.IsTrue(athletes.Count() > 0);
        }

        [TestMethod]
        [TestCategory("GetAllAthletes")]
        public void AthleteService_GetAllAthletes_Query_WithRequiredParam_ShouldReturn_Records()
        {
            IPagedList<AthleteModel> athletes = athleteServiceMock.GetAllAthletes("cooper", null, null, true, "",
                1, null, "name asc", 0, UserId, true, false, 1, 20);
            System.Diagnostics.Debug.WriteLine(string.Format("# of Records : {0}", athletes.Count));
            Assert.IsTrue(athletes.Count() > 0);
        }
    }
}
