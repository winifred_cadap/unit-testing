﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CAMS.DD.Core.ViewModels.Athletes;

namespace CAMS.DD.Tests.AthletesService
{
    [TestClass]
    public class AthleteServiceCRUDTest : AthleteServiceBase
    {
        [TestMethod]
        [TestCategory("SaveAthlete")]
        public void SaveAthlete_AddBasic_ShouldReturn_AthleteRecord()
        {
            AthleteModel NewAthleteModel = SetAthleteData();
            
            AthleteModel NewlyCreatedAthlete = athleteServiceMock.SaveAthlete(NewAthleteModel);
            Assert.IsTrue(NewlyCreatedAthlete.Id > 0);
            Assert.IsTrue(NewlyCreatedAthlete.FirstName == NewAthleteModel.FirstName);
            Assert.IsTrue(NewlyCreatedAthlete.Surname == NewAthleteModel.Surname);
        }

        [TestMethod]
        [TestCategory("SaveAthlete")]
        [ExpectedException(typeof(Exception))]
        public void SaveAthlete_SaveBlankName_ShouldThrowException()
        {
            AthleteModel NewAthleteModel = SetAthleteData();

            NewAthleteModel.FirstName = "";
            NewAthleteModel.Surname = "";

            try
            {
                AthleteModel NewlyCreatedAthlete = athleteServiceMock.SaveAthlete(NewAthleteModel);

            }
            catch (Exception ex)
            {
                Assert.AreEqual("An error occured while saving the Athlete", ex.Message);
                throw;
            }
        }

        [TestMethod]
        [TestCategory("SaveAthlete")]
        [ExpectedException(typeof(Exception))]
        public void SaveAthlete_DuplicateName_ShouldThrowException()
        {
            AthleteModel NewAthleteModel = SetAthleteData();

            try
            {
                AthleteModel NewlyCreatedAthlete1 = athleteServiceMock.SaveAthlete(NewAthleteModel);
                AthleteModel NewlyCreatedAthlete2 = athleteServiceMock.SaveAthlete(NewAthleteModel);
            }
            catch (Exception ex)
            {
                Assert.AreEqual("An error occured while saving the Athlete", ex.Message);
                throw;
            }
        }

        [TestMethod]
        [TestCategory("DeleteAthlete")]
        public void DeleteAthlete_ShouldReturnNull()
        {
            //Insert data
            AthleteModel NewAthleteModel = SetAthleteData();

            NewAthleteModel.FirstName = "Jason";
            NewAthleteModel.FirstName = "Mark";
            
            AthleteModel NewlyCreatedAthlete = athleteServiceMock.SaveAthlete(NewAthleteModel);

            //Delete data using the newly created Id
            athleteService.DeleteAthlete(NewlyCreatedAthlete.Id);

            //Retrieve to check if deleted and should return null
            AthleteModel DeletedAthlete = GetAthleteByUserId(NewlyCreatedAthlete.Id);

            //Check
            Assert.IsTrue(DeletedAthlete == null);
        }

        [TestMethod]
        [TestCategory("DeleteAthlete")]
        [ExpectedException(typeof(Exception))]
        public void DeleteAthlete_AthleteNotFound_ShouldThrowException()
        {
            try
            {
                athleteService.DeleteAthlete(-23);
            }
            catch (Exception ex)
            {
                Assert.AreEqual("Athlete not found", ex.Message);
                throw;
            }
        }

        //===============================================
        //Internal
        //===============================================

        private AthleteModel GetAthleteByUserId(int AthleteId)
        {
            return athleteService.GetAthleteByUserId(AthleteId);
        }

        private AthleteModel SetAthleteData()
        {
            AthleteModel NewAthleteModel = new AthleteModel()
            {
                FirstName = "",
                Surname = "",
                Address = "5 Station Street",
                City = "Richmond",
                StateId = 1,
                DateOfBirth = Convert.ToDateTime("1983-12-05 00:00:00.0000000"),
                Email = "baldo@gmail.com",
                Twitter = "_#twit_twit",
                Mobile = "0438 648 216",
                Gender = "Male",
                UserId = UserId,
                Weight = 220,
                Height = 232,
                License = "ECE-XX882-122",
                IsActive = true,
                Phone = "2233-332-323"
            };
            return NewAthleteModel;
        }
    }
}
