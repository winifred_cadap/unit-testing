﻿using System;
using CAMS.DD.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CAMS.DD.Core.Services.Interfaces;
using System.Collections.Generic;
using CAMS.DD.Core.ViewModels.Athletes;
using System.Linq;
using Moq;


namespace CAMS.DD.Tests.AthletesService
{
    public class AthleteServiceBase
    {
        public IAthleteService athleteService { get; set; }
        public Mock<ICdnService> mockICdnService {get;set;}
        public AthleteService athleteServiceMock { get; set; }

        public int UserId{get;set;}
        public int AthletId {get;set;}
        public string AthleteName {get;set;}
         
        public AthleteServiceBase()
        {
            mockICdnService = new Mock<ICdnService>();
            athleteServiceMock = new AthleteService(mockICdnService.Object);
            athleteService = new AthleteService();
        }

        public void InitTestData()
        {
            UserId = 1068;
            AthletId = 1;
            AthleteName = "billy";
        }
                
    }
}
